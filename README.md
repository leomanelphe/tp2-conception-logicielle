# tp2-conception-logicielle : How to run the code

Install dependencies with `pip install -r requirements.txt`

Run `python main.py` in terminal

Check `http://localhost:8000` on a web browser